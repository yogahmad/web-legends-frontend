import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Axios from "axios";
import {serverurl} from '../../Env';

export default function Login() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const history = useHistory();

  useEffect(() => {
    // COMPONENT DID MOUNT
    if (localStorage.getItem("token") !== null) {
      Axios.post(serverurl + "account/authenticate/", {
        token: localStorage.getItem("token")
      })
        .then(res => {
          if (res.data.token === localStorage.getItem("token")) {
            history.push("/login");
          } else {
            localStorage.removeItem("token");
            history.push("/");
          }
        })
        .catch(err => {
          alert(err);
        });
    }
  });

  const registerHandler = event => {
    // Post request register
    event.preventDefault();
    if (password !== repeatPassword) {
      alert("Password do not match");
      return;
    }
    Axios.post(serverurl + "account/register/", {
      username: username,
      email: email,
      password: password
    })
      .then(res => {
        history.push("/login");
      })
      .catch(err => {
        alert(err);
      });
  };

  return (
    <div className="d-flex justify-content-center align-items-center h-100">
      <div className="p-2 border border-info">
        <div className="row form-group m-2">
          <label className="col">Username: </label>
          <input
            className="form-control col"
            value={username}
            onChange={event => setUsername(event.target.value)}
          ></input>
        </div>
        <div className="row form-group m-2">
          <label className="col">Email: </label>
          <input
            className="form-control col"
            value={email}
            onChange={event => setEmail(event.target.value)}
          ></input>
        </div>
        <div className="row form-group m-2">
          <label className="col">Password: </label>
          <input
            className="form-control col"
            type="password"
            value={password}
            onChange={event => setPassword(event.target.value)}
          ></input>
        </div>
        <div className="row form-group m-2">
          <label className="col">Repeat Password: </label>
          <input
            className="form-control col"
            type="password"
            value={repeatPassword}
            onChange={event => setRepeatPassword(event.target.value)}
          ></input>
        </div>
        <div className="row form-group m-2">
          <button
            className="btn btn-secondary w-100"
            onClick={event => registerHandler(event)}
          >
            Register
          </button>
        </div>
      </div>
    </div>
  );
}
