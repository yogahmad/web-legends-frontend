import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Axios from "axios";
import { serverurl } from "../../Env";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoaded, setLoaded] = useState(false);
  const history = useHistory();

  useEffect(() => {
    // COMPONENT DID MOUNT
    if (localStorage.getItem("token") !== null) {
      Axios.post(serverurl + "account/authenticate/", {
        token: localStorage.getItem("token")
      })
        .then(res => {
          if (res.data.token === localStorage.getItem("token")) {
            history.push("/");
          } else {
            localStorage.removeItem("token");
            setLoaded(true);
          }
        })
        .catch(err => {
          alert(err);
        });
    } else setLoaded(true);
  }, []);

  const loginHandler = event => {
    // Post login register
    event.preventDefault();
    Axios.post(serverurl + "account/login/", {
      username: username,
      password: password
    })
      .then(res => {
        localStorage.setItem("token", res.data.token);
        history.push("/");
      })
      .catch(err => {
        alert(err);
      });
  };

  return (
    <div className="d-flex justify-content-center align-items-center h-100">
      {isLoaded ? (
        <div className="p-2 border border-info">
          <div className="row form-group m-2">
            <label className="col">Username: </label>
            <input
              className="form-control col"
              value={username}
              onChange={event => setUsername(event.target.value)}
            ></input>
          </div>
          <div className="row form-group m-2">
            <label className="col">Password: </label>
            <input
              className="form-control col"
              type="password"
              value={password}
              onChange={event => setPassword(event.target.value)}
            ></input>
          </div>
          <div className="row form-group m-2">
            <button
              className="btn btn-secondary w-100"
              onClick={event => loginHandler(event)}
            >
              Login
            </button>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
