import Character from "./Character";

export class Skill {
  constructor(owner, target, manaRequired, power) {
    this.target = target;
    this.owner = owner;
    this.manaRequired = manaRequired;
    this.power = power;
  }
}

export class HealHP extends Skill {
  constructor(owner, target, manaRequired, power) {
    super(owner, target, manaRequired, power);
  }

  use = () => {
    if (this.manaRequired > this.owner.MP) return;
    this.owner.setHP(this.power);
    this.owner.setMP(-this.manaRequired);
  };

  toString = () => {
    return "+" + this.power + " own's HP and" + " -" + this.manaRequired + " own's MP";
  }
}

export class HealMP extends Skill {
  constructor(owner, target, manaRequired, power) {
    super(owner, target, manaRequired, power);
  }

  use = () => {
    if (this.manaRequired > this.owner.MP) return;
    this.owner.setMP(this.power);
    this.owner.setMP(-this.manaRequired);
  };

  toString = () => {
    return "+" + this.power + " own's MP and " + "-" + this.manaRequired + " own's MP";
  }
}

export class Attack extends Skill {
  constructor(owner, target, manaRequired, power) {
    super(owner, target, manaRequired, power);
  }

  use = () => {
    if (this.manaRequired > this.owner.MP) return;
    this.target.setHP(-this.power);
    this.owner.setMP(-this.manaRequired);
  };

  toString = () => {
    return "-" + this.power + " enemy's HP and" + " -" + this.manaRequired + " own's MP";
  }
}
