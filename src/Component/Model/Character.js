export class Character {
  constructor(name, maxHP, maxMP) {
    this.name = name;
    this.HP = maxHP;
    this.MP = maxMP;
    this.maxHP = maxHP;
    this.maxMP = maxMP;
    this.skill = [];
  }

  setHP = change => {
    let newHP = this.HP + change;
    if (newHP < 0) newHP = 0;
    if (newHP > this.maxHP) newHP = this.maxHP;
    this.HP = newHP;
  };

  setMP = change => {
    let newMP = this.MP + change;
    if (newMP < 0) newMP = 0;
    if (newMP > this.maxMP) newMP = this.maxMP;
    this.MP = newMP;
  };

  addSkill = newSkill => {
    this.skill.push(newSkill);
  };

  useSkill = id => {
    this.skill[id].use();
  };

  toString = () => {
    return this.name + " has " + this.HP + "HP and " + this.MP + "MP";
  }
}
