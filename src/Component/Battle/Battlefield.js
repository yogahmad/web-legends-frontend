import React, { useState, useEffect, useCallback } from "react";
import { useParams } from "react-router-dom";
import ProgressBar from "react-bootstrap/ProgressBar";
import "./Battlefield.css";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { serverurl } from "../../Env";
import { Character } from "../Model/Character";
import { HealHP, HealMP, Attack } from "../Model/Skill";

export default function Battlefield() {
  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);
  // PLAYER STATE
  const [player, setPlayer] = useState(null);

  // ENEMY STATE
  const [enemy, setEnemy] = useState(null);

  //STATE
  const [isLoaded, setIsLoaded] = useState(false);
  const [isReady, setIsReady] = useState(false);

  // TURN 0 PLAYER, 1 ENEMY
  const [turn, setTurn] = useState(0);

  const { id } = useParams();
  const history = useHistory();

  // TO:DO Get from server maxHP and maxMP in the start of the game
  useEffect(() => {
    if (isReady) return;
    if (localStorage.getItem("token") !== null) {
      Axios.post(serverurl + "account/authenticate/", {
        token: localStorage.getItem("token")
      })
        .then(res => {
          if (res.data.token === localStorage.getItem("token")) {
            //TO:DO
          } else {
            localStorage.removeItem("token");
            history.push("/");
          }
        })
        .catch(err => {
          alert(err);
        });
    } else history.push("/");
    setIsReady(true);

    const loadBattleData = () => {
      //LOAD PLAYER
      const newPlayer = new Character("Player", 3700, 1000);
      const newEnemy = new Character("Enemy", 4500, 700);
      //ADD SKILL
      newPlayer.addSkill(new HealHP(newPlayer, newEnemy, 200, 500));
      newPlayer.addSkill(new HealMP(newPlayer, newEnemy, 0, 300));
      newPlayer.addSkill(new Attack(newPlayer, newEnemy, 0, 300));
      newPlayer.addSkill(new Attack(newPlayer, newEnemy, 300, 700));
      newPlayer.addSkill(new Attack(newPlayer, newEnemy, 400, 1000));
      //ADD ENEMY SKILL
      newEnemy.addSkill(new HealHP(newEnemy, newPlayer, 150, 400));
      newEnemy.addSkill(new HealMP(newEnemy, newPlayer, 0, 400));
      newEnemy.addSkill(new Attack(newEnemy, newPlayer, 0, 200));
      newEnemy.addSkill(new Attack(newEnemy, newPlayer, 250, 800));

      setEnemy(newEnemy);
      setPlayer(newPlayer);
      setIsLoaded(true);
    };

    loadBattleData();
  }, []);

  return (
    <div className="w-100 h-100">
      {isLoaded ? (
        <div className="w-100 h-100">
          <div className="row w-100 h-50">
            <nav className="navbar navbar-expand-lg navbar-light bg-light m-2">
              <a className="navbar-brand" href="#">
                Stage {id}
              </a>
            </nav>
            <div className="w-100 d-flex justify-content-center">
              <h2>{turn === 0 ? "Player's turn" : "Enemy's turn"}</h2>
            </div>

            <div className="row w-100 h-50">
              <div className="col-3"></div>
              <div className="col d-flex flex-row align-items-center">
                <div className="w-100">
                  <div className="w-100 d-flex flex-row justify-content-around mr-2">
                    {enemy.skill.map((skill, index) => {
                      return (
                        <div
                          className="skillbox m-2"
                          key={index}
                          title={skill.toString()}
                          data-toggle="tooltip"
                          onClick={() => {
                            if (turn === 1) {
                              skill.use();
                              setTurn(1 - turn);
                              forceUpdate();
                            }
                          }}
                        ></div>
                      );
                    })}
                  </div>
                  <ProgressBar className="m-2">
                    <ProgressBar
                      now={enemy.HP}
                      variant="success"
                      label={enemy.HP}
                    />
                    <ProgressBar
                      now={enemy.maxHP - enemy.HP}
                      variant="danger"
                      label={enemy.maxHP - enemy.HP}
                    />
                  </ProgressBar>
                  <ProgressBar className="m-2">
                    <ProgressBar
                      now={enemy.MP}
                      variant="info"
                      label={enemy.MP}
                    />
                    <ProgressBar
                      now={enemy.maxMP - enemy.MP}
                      variant="secondary"
                      label={enemy.maxMP - enemy.MP}
                    />
                  </ProgressBar>
                </div>
              </div>
              <div className="col-3"></div>
            </div>
            <div className="row w-100 h-50"></div>
          </div>
          <div className="row w-100 h-50">
            <div className="row w-100 h-50"></div>
            <div className="row w-100 h-50">
              <div className="col-3"></div>
              <div className="col d-flex flex-row align-items-center">
                <div className="w-100">
                  <div className="w-100 d-flex flex-row justify-content-around mr-2">
                    {player.skill.map((skill, index) => {
                      return (
                        <div
                          className="skillbox m-2"
                          key={index}
                          title={skill.toString()}
                          data-toggle="tooltip"
                          onClick={() => {
                            if (turn === 0) {
                              skill.use();
                              setTurn(1 - turn);
                              forceUpdate();
                            }
                          }}
                        ></div>
                      );
                    })}
                  </div>
                  <ProgressBar className="m-2">
                    <ProgressBar
                      now={player.HP}
                      variant="success"
                      label={player.HP}
                    />
                    <ProgressBar
                      now={player.maxHP - player.HP}
                      variant="danger"
                      label={player.maxHP - player.HP}
                    />
                  </ProgressBar>
                  <ProgressBar className="m-2">
                    <ProgressBar
                      now={player.MP}
                      variant="info"
                      label={player.MP}
                    />
                    <ProgressBar
                      now={player.maxMP - player.MP}
                      variant="secondary"
                      label={player.maxMP - player.MP}
                    />
                  </ProgressBar>
                </div>
              </div>
              <div className="col-3"></div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
