import React, { useState, useEffect } from "react";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { serverurl } from "../../Env";

export default function Create() {
  const [char, setChar] = useState("none");
  const [weapon, setWeapon] = useState("none");
  const [skill, setSkill] = useState("none");
  const [isLoaded, setLoaded] = useState(false);
  const history = useHistory();

  useEffect(() => {
    //COMPONENT DID MOUNT
    if (localStorage.getItem("token") !== null) {
      Axios.post(serverurl + "account/authenticate/", {
        token: localStorage.getItem("token")
      })
        .then(async res => {
          if (res.data.token === localStorage.getItem("token")) {
            const result = await Axios.post(serverurl + "account/getscore", {
              token: localStorage.getItem("token")
            });
            if (result.data.gameId !== "none") {
              history.push("/level/" + (parseInt(result.data.score) + 1));
            } else {
              setLoaded(true);
            }
          } else {
            localStorage.removeItem("token");
            history.push("/");
          }
        })
        .catch(err => {
          alert(err);
        });
    } else history.push("/");
  });

  const selectCharacterHandler = newValue => {
    setChar(newValue);
    selectWeaponHandler("none");
    document.getElementById("selectweapon").style.display = "flex";
    selectSkillHandler("none");
    document.getElementById("selectskill").style.display = "none";
  };

  const selectWeaponHandler = newValue => {
    setWeapon(newValue);
    setSkill("none");
    document.getElementById("selectskill").style.display = "flex";
  };

  const selectSkillHandler = newValue => {
    setSkill(newValue);
  };

  const clickHandler = async event => {
    event.preventDefault();
    // POST REQUEST API CREATE CHARACTER, SKILL, WEAPON
    const result = await Axios.post(serverurl + "account/create/", {
      token: localStorage.getItem("token"),
      char: char
    }).catch(err => alert(err));
    console.log(result);
    // IF VALID THEN GO TO THE FIRST ROUND
    history.push("/level/1");
  };

  return (
    <div className="d-flex justify-content-center align-items-center h-100">
      {isLoaded ? (
        <div className="p-2 border border-info">
          <div className="row form-group m-2">
            <label className="col">Select Character: </label>
            <select
              className="form-control col"
              style={{ minWidth: "150px" }}
              onChange={event => selectCharacterHandler(event.target.value)}
              value={char}
            >
              <option value="none" style={{ display: "none" }}></option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div
            className="row form-group m-2"
            style={{ display: "none" }}
            id="selectweapon"
          >
            <label className="col">Select Weapon: </label>
            <select
              className="form-control col"
              style={{ minWidth: "150px" }}
              onChange={event => selectWeaponHandler(event.target.value)}
              value={weapon}
            >
              <option value="none" style={{ display: "none" }}></option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div
            className="row form-group m-2"
            style={{ display: "none" }}
            id="selectskill"
          >
            <label className="col">Select Skill: </label>
            <select
              className="form-control col"
              style={{ minWidth: "150px" }}
              onChange={event => selectSkillHandler(event.target.value)}
              value={skill}
            >
              <option value="none" style={{ display: "none" }}></option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div className="row form-group m-2">
            <button
              className="btn btn-secondary w-100"
              onClick={event => clickHandler(event)}
            >
              Start Game
            </button>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
