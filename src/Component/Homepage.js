import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Axios from "axios";
import { serverurl } from "../Env";

export default function Home() {
  const history = useHistory();
  const [isAuthenticated, setAuthenticated] = useState(false);
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    // COMPONENT DID MOUNT
    async function authenticate() {
      if (localStorage.getItem("token") !== null) {
        await Axios.post(serverurl + "account/authenticate/", {
          token: localStorage.getItem("token")
        })
          .then(async res => {
            if (res.data.token === localStorage.getItem("token")) {
              setAuthenticated(true);
            } else {
              localStorage.removeItem("token");
              history.push("/");
            }
          })
          .catch(err => {
            localStorage.removeItem("token");
            history.push("/");
          });
      }
      setLoaded(true);
    }
    authenticate();
  }, []);

  return (
    <div>
      {isLoaded ? (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark m-0">
          <a className="navbar-brand" href="/">
            Web Legends
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto"></ul>
            {isAuthenticated ? (
              <div>
                <button
                  className="btn btn-outline-primary m-2 my-sm-0"
                  onClick={() => {
                    history.push("/create");
                  }}
                >
                  Create Character
                </button>
                <button
                  className="btn btn-outline-primary m-2 my-sm-0"
                  onClick={() => {
                    localStorage.removeItem("token");
                    window.location.reload();
                  }}
                >
                  Logout
                </button>
              </div>
            ) : (
              <div>
                <button
                  className="btn btn-outline-primary m-2 my-sm-0"
                  onClick={() => history.push("/login")}
                >
                  Login
                </button>
                <button
                  className="btn btn-outline-primary m-2 my-sm-0"
                  onClick={() => history.push("/signup")}
                >
                  Register
                </button>
              </div>
            )}
          </div>
        </nav>
      ) : (
        ""
      )}
      {isLoaded ? (
        <div className="row">
          <div className="col-md-9 d-flex justify-content-center align-items-center bg-secondary mt-5">
            <button className="btn btn-outline-primary">Play for free</button>
          </div>
          <div className="col-md-3">
            <div className="col">
              <table className="table table-bordered mt-5 mb-0">
                <thead>
                  <tr className="table-secondary">
                    <th scope="col" style={{ textAlign: "center" }}>
                      #
                    </th>
                    <th scope="col" style={{ textAlign: "center" }}>
                      Username
                    </th>
                    <th scope="col" style={{ textAlign: "center" }}>
                      Score
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="table-primary">
                    <th scope="row" style={{ textAlign: "center" }}>
                      1
                    </th>
                    <td style={{ textAlign: "center" }}>yogahmad</td>
                    <td style={{ textAlign: "center" }}>10</td>
                  </tr>
                  <tr className="table-info">
                    <th scope="row" style={{ textAlign: "center" }}>
                      2
                    </th>
                    <td style={{ textAlign: "center" }}>yogahmad</td>
                    <td style={{ textAlign: "center" }}>10</td>
                  </tr>
                  <tr className="table-primary">
                    <th scope="row" style={{ textAlign: "center" }}>
                      3
                    </th>
                    <td style={{ textAlign: "center" }}>yogahmad</td>
                    <td style={{ textAlign: "center" }}>10</td>
                  </tr>
                  <tr className="table-info">
                    <th scope="row" style={{ textAlign: "center" }}>
                      4
                    </th>
                    <td style={{ textAlign: "center" }}>yogahmad</td>
                    <td style={{ textAlign: "center" }}>10</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
