import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./Component/Account/Login";
import Signup from "./Component/Account/Signup";
import Battlefield from "./Component/Battle/Battlefield";
import Create from "./Component/Battle/Create";
import Homepage from "./Component/Homepage";


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {};

  render() {
    return (
      <Router>
        <div className="h-100">
          <Switch>
            <Route path="/login">
              <Login></Login>
            </Route>
            <Route path="/signup">
              <Signup></Signup>
            </Route>
            <Route path="/level/:id">
              <Battlefield></Battlefield>
            </Route>
            <Route path="/create">
              <Create></Create>
            </Route>
            <Route path="/">
              <Homepage></Homepage>
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
